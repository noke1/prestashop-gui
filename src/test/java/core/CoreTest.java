package core;

import helper.browser.Browser;
import helper.browser.BrowserAdjust;
import helper.browser.BrowserTerminate;
import helper.browser.ChromeInitialize;
import helper.others.BrowserTools;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

public class CoreTest {

    private static WebDriver driver;
    private Browser browser;
    private BrowserTools browserTools;

    public CoreTest() {
        browser = new Browser(
                "Chrome",
                new ChromeInitialize(),
                new BrowserTerminate(),
                new BrowserAdjust()
        );
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public BrowserTools getBrowserTools() {
        return browserTools;
    }

    @BeforeSuite
    public void beforeSuite() {
        driver = browser.initialize();
    }

    @BeforeMethod
    public void beforeMethod(ITestContext context) {
        //so i can use it inside TestListener class to make a screenshot for example.
        context.setAttribute("webDriver", getDriver());

        browser.adjust(getDriver());
        browserTools = new BrowserTools(getDriver());
    }

    @AfterSuite
    public void afterSuite() {
        browser.terminate(getDriver());
    }
}
