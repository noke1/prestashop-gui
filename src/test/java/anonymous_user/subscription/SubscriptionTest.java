package anonymous_user.subscription;

import com.github.javafaker.Faker;
import core.CoreTest;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;
import pages.commonpages.CommonFooterPage;

import java.util.Locale;

public class SubscriptionTest extends CoreTest {

    private CommonFooterPage commonFooterPage;

    @Test(description = "PRESTA-45")
    public void validationAlertAppears_EmptyEmailTest(){
        commonFooterPage = new CommonFooterPage(getDriver());

        commonFooterPage.signForSubscriptionWithEmail(false);

        Assertions.assertThat(commonFooterPage.isAlertInvalidEmailForSubscribeVisibleAndCorrect()).
                withFailMessage("Alert is not visible or text don\'t match").
                isTrue();
    }


    @Test(description = "PRESTA-26")
    public void successAlertAppears_ValidEmailTest(){
        commonFooterPage = new CommonFooterPage(getDriver());

        commonFooterPage.signForSubscriptionWithEmail(true);

        Assertions.assertThat(commonFooterPage.isAlertSuccessfulSubscribeVisibleAndCorrect()).
                withFailMessage("Alert is not visible or text don\'t match").
                isTrue();
    }


    @Test(description = "PRESTA-28")
    public void validationAlertAppears_AlreadyInDataBaseEmailTest(){
        Faker faker = new Faker(new Locale("pl-PL"));
        String email = faker.internet().safeEmailAddress();
        commonFooterPage = new CommonFooterPage(getDriver());

        commonFooterPage.
                createCustomerWithEmail(email).
                createCustomerWithEmail(email);

        Assertions.assertThat(commonFooterPage.isAlertAlreadySubscribedVisibleAndCorrect()).
                withFailMessage("Alert is not visible or text don\'t match").
                isTrue();
    }


    @Test(description = "PRESTA-27")
    public void validationAlertAppears_InvalidEmailFormatTest() {
        //TODO
//        commonFooterPage = new CommonFooterPage(getDriver());
//        commonFooterPage.signForSubscriptionWithEmail("xdxd8@xd.pl");
    }
}
