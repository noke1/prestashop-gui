package anonymous_user.clothes;

import core.CoreTest;
import org.testng.annotations.Test;
import pages.clothespage.AvailableClothesPage;
import pages.clothespage.FiltersClothesPage;
import pages.commonpages.CommonNavbarPage;

import static org.assertj.core.api.Assertions.assertThat;
import static pages.clothespage.FiltersClothesPage.SubCategory.MEN;
import static pages.clothespage.FiltersClothesPage.SubCategory.WOMEN;
import static pages.commonpages.CommonNavbarPage.Category.CLOTHES;


public class ClothesBrowseTest extends CoreTest {

    private CommonNavbarPage commonNavbarPage;
    private FiltersClothesPage filtersClothesPage;
    private AvailableClothesPage availableClothesPage;


    @Test(description = "PRESTA-6")
    public void moreThan0MenClothesAppear_BrowseMenCategoryTest() {
        prepareTestComponents();

        commonNavbarPage.goToCategory(CLOTHES);
        filtersClothesPage.goToSubcategory(MEN);

        assertThat(availableClothesPage.countClothesFound())
                .as("check if any MEN clothes were found")
                .isGreaterThan(0);
    }

    @Test(description = "PRESTA-7")
    public void moreThan0WomenClothesAppear_BrowseWomenCategoryTest(){
        prepareTestComponents();

        commonNavbarPage.goToCategory(CLOTHES);
        filtersClothesPage.goToSubcategory(WOMEN);

        assertThat(availableClothesPage.countClothesFound())
                .as("check if any WOMEN clothes were found")
                .isGreaterThan(0);
    }

    private void prepareTestComponents() {
        commonNavbarPage = new CommonNavbarPage(getDriver());
        filtersClothesPage = new FiltersClothesPage(getDriver());
        availableClothesPage = new AvailableClothesPage(getDriver());
    }

}
