package anonymous_user.clothes;

import core.CoreTest;
import org.testng.annotations.Test;
import pages.clothespage.AvailableClothesPage;
import pages.clothespage.FiltersClothesPage;
import pages.commonpages.CommonNavbarPage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static pages.clothespage.AvailableClothesPage.SortType.*;
import static pages.clothespage.FiltersClothesPage.SubCategory.MEN;
import static pages.commonpages.CommonNavbarPage.Category.CLOTHES;


public class ClothesSortTest extends CoreTest {

    private CommonNavbarPage commonNavbarPage;
    private FiltersClothesPage filtersClothesPage;
    private AvailableClothesPage availableClothesPage;


    @Test(description = "PRESTA-8")
    public void sortClothesByPriceDescTest() {
        prepareTestComponents();

        commonNavbarPage.goToCategory(CLOTHES);
        filtersClothesPage.goToSubcategory(MEN);
        availableClothesPage.sortClothesBy(PRICE_DESC);

        List<Double> clothesPrices = availableClothesPage.getClothesPrices();
        availableClothesPage.reverseList(clothesPrices);
        assertThat(clothesPrices)
                .as("check if products prices are sorted by PRICE_DESC (high to low)")
                .isNotNull()
                .isSorted();
    }

    @Test(description = "PRESTA-9")
    public void sortClothesByPriceAscTest() {
        prepareTestComponents();

        commonNavbarPage.goToCategory(CLOTHES);
        filtersClothesPage.goToSubcategory(MEN);
        availableClothesPage.sortClothesBy(PRICE_ASC);

        List<Double> clothesPrices = availableClothesPage.getClothesPrices();
        assertThat(clothesPrices)
                .as("check if products prices are sorted by PRICE_ASC (low to high)")
                .isNotNull()
                .isSorted();
    }

    @Test(description = "PRESTA-57")
    public void sortClothesByNameDescTest() {
        prepareTestComponents();

        commonNavbarPage.goToCategory(CLOTHES);
        filtersClothesPage.goToSubcategory(MEN);
        availableClothesPage.sortClothesBy(NAME_DESC);

        List<String> clothesNames = availableClothesPage.getClothesNames();
        availableClothesPage.reverseList(clothesNames);
        assertThat(clothesNames)
                .as("check if products prices are sorted by NAME_DESC (Z to A)")
                .isNotNull()
                .isSorted();
    }

    @Test(description = "PRESTA-56")
    public void sortClothesByNameAscTest() {
        prepareTestComponents();

        commonNavbarPage.goToCategory(CLOTHES);
        filtersClothesPage.goToSubcategory(MEN);
        availableClothesPage.sortClothesBy(NAME_ASC);

        List<String> clothesNames = availableClothesPage.getClothesNames();
        assertThat(clothesNames)
                .as("check if products prices are sorted by NAME_ASC (A to Z)")
                .isNotNull()
                .isSorted();
    }

    @Test(description = "PRESTA-58")
    public void sortClothesByDostepnyTest() {
        prepareTestComponents();

        commonNavbarPage.goToCategory(CLOTHES);
        filtersClothesPage.goToSubcategory(MEN);
        availableClothesPage.sortClothesBy(DOSTEPNE);

        assertThat(availableClothesPage.getClothesNames())
                .as("check if products quantity are sorted from high to low")
                .isNotNull()
                .startsWith("hummingbird printed t-shirt")
                .endsWith("fuckm123123e");
    }

    private void prepareTestComponents() {
        commonNavbarPage = new CommonNavbarPage(getDriver());
        filtersClothesPage = new FiltersClothesPage(getDriver());
        availableClothesPage = new AvailableClothesPage(getDriver());
    }


}
