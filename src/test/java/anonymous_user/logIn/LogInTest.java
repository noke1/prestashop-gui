package anonymous_user.logIn;

import core.CoreTest;
import object.generator.CustomerGenerator;
import object.pojo.Customer;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;
import pages.commonpages.CommonNavbarPage;
import pages.createaccountpage.CreateAccountPage;
import pages.loginpage.LogInPage;

public class LogInTest extends CoreTest {

    private CommonNavbarPage commonNavbarPage;
    private LogInPage logInPage;
    private CreateAccountPage createAccountPage;


    @Test(description = "PRESTA-23")
    public void customerPageAppears_LogInWithCorrectCredentialsTest() {
        Customer randomCustomer = new CustomerGenerator().generateData();
        prepareTestComponents();

        createAccountAndGoToLoginPage(randomCustomer);
        logInPage
                .fillFormWithEmailAndPassword(randomCustomer)
                .submitLogIn();

        Assertions.assertThat(commonNavbarPage.isLogOutButtonVisible())
                .withFailMessage("Log out button is not visible")
                .isTrue();
    }


    @Test(description = "PRESTA-24")
    public void validationMessageAppears_LogInWithInvalidEmailAndValidPasswordTest() {
        Customer randomCustomer = new CustomerGenerator().generateData();
        prepareTestComponents();

        createAccountAndGoToLoginPage(randomCustomer);
        logInPage
                .fillFormWithEmailAndPassword("xxx" + randomCustomer.getEmail(), randomCustomer.getPassword())
                .submitLogIn();

        Assertions.assertThat(logInPage.isLogAlertInvalidCredentialsVisibleAndCorrect())
                .withFailMessage("Message is not visible or text don\'t match")
                .isTrue();
    }


    @Test(description = "PRESTA-25")
    public void validationMessageAppears_LogInWithValidEmailAndInvalidPasswordTest() {
        Customer randomCustomer = new CustomerGenerator().generateData();
        prepareTestComponents();

        createAccountAndGoToLoginPage(randomCustomer);
        logInPage
                .fillFormWithEmailAndPassword(randomCustomer.getEmail(), randomCustomer.getPassword()+"xxx")
                .submitLogIn();

        Assertions.assertThat(logInPage.isLogAlertInvalidCredentialsVisibleAndCorrect())
                .withFailMessage("Message is not visible or text don\'t match")
                .isTrue();
    }

    @Test(description = "PRESTA-48")
    public void validationMessageAppears_LogInToNonCreatedAccountTest() {
        Customer randomCustomer = new CustomerGenerator().generateData();
        prepareTestComponents();

        commonNavbarPage.goToLogInPage();
        logInPage
                .fillFormWithEmailAndPassword(randomCustomer)
                .submitLogIn();

        Assertions.assertThat(logInPage.isLogAlertInvalidCredentialsVisibleAndCorrect())
                .withFailMessage("Message is not visible or text don\'t match")
                .isTrue();
    }

    private void prepareTestComponents() {
        logInPage = new LogInPage(getDriver());
        commonNavbarPage = new CommonNavbarPage(getDriver());
        createAccountPage = new CreateAccountPage(getDriver());
    }

    private void createAccountAndGoToLoginPage(Customer randomCustomer) {
        commonNavbarPage.goToLogInPage();
        logInPage.goToCreateAccountPage();
        createAccountPage
                .fillFormWithMinimalData(randomCustomer)
                .submitRegistration();
        commonNavbarPage.logOut();
        commonNavbarPage.goToLogInPage();
    }

}
