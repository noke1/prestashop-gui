package anonymous_user.contactUs;

import core.CoreTest;
import object.generator.ContactUsMessageGenerator;
import object.pojo.ContactUsMessage;
import org.assertj.core.api.Assertions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.commonpages.CommonNavbarPage;
import pages.contactuspage.ContactUsPage;

import static pages.contactuspage.ContactUsPage.ContactUsTopic.BOK;
import static pages.contactuspage.ContactUsPage.ContactUsTopic.WEBMASTER;

public class ContactUsTest extends CoreTest {

    private CommonNavbarPage commonNavbarPage;
    private ContactUsPage contactUsPage;
    private ContactUsMessage randomlyGeneratedMessage;


    @Test(description = "PRESTA-3")
    public void confirmationMessageIsAppears_SendMessageToBOKWithMinimalDataTest() {
        prepareTestComponents();

        commonNavbarPage.goToContactUsPage();
        contactUsPage
                .selectMessageTopic(BOK)
                .fillFormWithMinimalData(randomlyGeneratedMessage)
                .sendMessage();

        Assertions.assertThat(contactUsPage.isAlertSuccessfulMessageSendVisibleAndCorrect())
                .isTrue();
    }


    @Test(description = "PRESTA-49")
    public void confirmationMessageIsAppears_SendMessageToWebmasterWithMinimalDataTest() {
        prepareTestComponents();

        commonNavbarPage.goToContactUsPage();
        contactUsPage
                .selectMessageTopic(WEBMASTER)
                .fillFormWithMinimalData(randomlyGeneratedMessage)
                .sendMessage();

        Assertions.assertThat(contactUsPage.isAlertSuccessfulMessageSendVisibleAndCorrect())
                .isTrue();
    }


    @Test(dataProvider = "validAttachmentsFormats", description = "PRESTA-4")
    public void confirmationMessageAppears_SendMessageWithValidAttachmentTest(String pathToAttachment) {
        prepareTestComponents();

        commonNavbarPage.goToContactUsPage();
        contactUsPage
                .selectMessageTopic(WEBMASTER)
                .fillFormWithMinimalData(randomlyGeneratedMessage)
                .addAttachment(pathToAttachment)
                .sendMessage();

        Assertions.assertThat(contactUsPage.isAlertSuccessfulMessageSendVisibleAndCorrect())
                .isTrue();
    }


    @Test(dataProvider = "invalidAttachmentsFormats", description = "PRESTA-50")
    public void validationMessageAppears_SendMessageWithInvalidAttachmentTest(String pathToAttachment) {
        prepareTestComponents();

        commonNavbarPage.goToContactUsPage();
        contactUsPage
                .selectMessageTopic(WEBMASTER)
                .fillFormWithMinimalData(randomlyGeneratedMessage)
                .addAttachment(pathToAttachment)
                .sendMessage();

        Assertions.assertThat(contactUsPage.isAlertInvalidAttachmentFormatVisibleAndCorrect())
                .isTrue();
    }

    private void prepareTestComponents() {
        commonNavbarPage = new CommonNavbarPage(getDriver());
        contactUsPage = new ContactUsPage(getDriver());
        randomlyGeneratedMessage = new ContactUsMessageGenerator().generateData();
    }

    @DataProvider(name = "invalidAttachmentsFormats")
    public Object[][] invalidAttachmentsFormats() {
        return new Object[][] {
                {"ss.xcf"},
                {"ss.tar.gz"}
        };
    }

    @DataProvider(name = "validAttachmentsFormats")
    public Object[][] validAttachmentsFormats() {
        return new Object[][] {
                {"ss.png"},
                {"ss.jpg"}
        };
    }

}
