package anonymous_user.createAccount;

import core.CoreTest;
import helper.excel.ExcelReader;
import object.generator.CustomerGenerator;
import object.pojo.Customer;
import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.commonpages.CommonNavbarPage;
import pages.createaccountpage.CreateAccountPage;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateAccountTest extends CoreTest {

    private CommonNavbarPage commonNavbarPage;
    private CreateAccountPage createAccountPage;


    @Test(description = "PRESTA-30")
    public void newCustomerIsCreated_MinimalValidDataTest()  {
        Customer randomCustomer = new CustomerGenerator().generateData();
        prepareTestComponents();

        createAccountPage
                .fillFormWithMinimalData(randomCustomer)
                .submitRegistration();

        assertThat(commonNavbarPage.isLogOutButtonVisible())
                .withFailMessage("Log out button is not visible")
                .isTrue();
    }


    @Test(dataProvider = "firstnameLastnameValidationData", description = "PRESTA-47")
    public void firstnameLastnameValidationMessages_MinimalInvalidDataTest(String firstname, String lastname, String email, String password)  {
        prepareTestComponents();

        createAccountPage
                .fillFormWithMinimalData(firstname, lastname, email, password)
                .submitRegistration();

        assertThat(createAccountPage.isFirstnameLastnameValidationMessagesVisibleAndCorrect())
                .withFailMessage("Alerts are not visible or text don\'t match")
                .isTrue();
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(createAccountPage.getFirstnameLastnameValidationMessages().size())
                .as("Validation messages count")
                .isEqualTo(2);
        softly.assertAll();
    }

    private void prepareTestComponents() {
        createAccountPage = new CreateAccountPage(getDriver());
        commonNavbarPage = new CommonNavbarPage(getDriver());
        getBrowserTools().goTo("http://localhost:8080/logowanie?create_account=1");
    }

    @DataProvider(name = "firstnameLastnameValidationData")
    public Object[][] firstnameLastnameValidationData() throws IOException {
        return new ExcelReader().getTestDataFromExcel("/home/mat/Documents/projects/prestashop-gui/src/main/resources/test_data/test_data.xlsx","firstnameLastnameValidationData");
    }


}
