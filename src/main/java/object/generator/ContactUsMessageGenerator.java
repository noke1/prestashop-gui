package object.generator;

import object.pojo.ContactUsMessage;

public class ContactUsMessageGenerator implements Generator<ContactUsMessage>{


    @Override
    public ContactUsMessage generateData(){
        return new ContactUsMessage(
                generateEmail(),
                generateMessageText()
        );
    }

    private String generateMessageText() {
        return faker.numerify("TEST###TEST");
    }

    private String generateEmail() {
        return faker.internet().safeEmailAddress();
    }

}
