package object.generator;

import object.pojo.Customer;

public class CustomerGenerator implements Generator<Customer> {


    @Override
    public Customer generateData(){
        return new Customer(
                generateFirstname(),
                generateLastName(),
                generateEmail(),
                generatePassword()
        );
    }

    private String generateFirstname() {
        return faker.name().firstName();
    }

    private String generateLastName() {
        return faker.name().lastName();
    }

    private String generateEmail() {
        return faker.internet().safeEmailAddress();
    }

    private String generatePassword() {
        return faker.internet().password();
    }

}
