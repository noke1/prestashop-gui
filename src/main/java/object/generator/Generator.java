package object.generator;

import com.github.javafaker.Faker;

import java.util.Locale;

public interface Generator<T> {

    Faker faker = new Faker(new Locale("pl-PL"));

    T generateData();
}
