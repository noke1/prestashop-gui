package object.pojo;

public class ContactUsMessage {

    private String email;
    private String messageText;

    public ContactUsMessage(String email, String messageText) {
        this.email = email;
        this.messageText = messageText;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    @Override
    public String toString() {
        return "Message{" +
                "email='" + email + '\'' +
                ", messageText='" + messageText + '\'' +
                '}';
    }
}
