package pages.commonpages;

import helper.others.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.corepage.CorePage;

import java.util.List;

import static helper.others.Waiter.*;

public class CommonNavbarPage extends CorePage {

    public CommonNavbarPage(WebDriver driver) {
        super(driver);
    }

    @FindBy (css = "div.user-info a span")
    private WebElement buttonLogIn;

    @FindBy (css = "a.logout.hidden-sm-down")
    private WebElement buttonLogOut;

    @FindBy (css = "#contact-link > a")
    private WebElement buttonContactUs;

    @FindBy (css = "#top-menu > li > a")
    private List<WebElement> listAllCategories;

    @FindBy (css = "a.account > span")
    private WebElement anchorCustomerCredentials;

    public enum Category {
        CLOTHES,
        PRODUKTY_POWIAZANE,
        ART
    }

    public void goToLogInPage() {
        waitForElementToAppear(buttonLogIn);
        buttonLogIn.click();
    }

    public void goToCategory(Category category) {
        switch (category) {
            case CLOTHES:
                waitAndGoTo(listAllCategories,0);
                break;
            case PRODUKTY_POWIAZANE:
                waitAndGoTo(listAllCategories,1);
                break;
            case ART:
                waitAndGoTo(listAllCategories, 2);
                break;
        }
    }

    public void waitAndGoTo(List<WebElement> list, int i) {
        waitForElementToAppear(list.get(i));
        list.get(i).click();
    }

    public void goToContactUsPage() {
        waitForElementToAppear(buttonContactUs);
        buttonContactUs.click();
    }

    public boolean isLogOutButtonVisible() {
        waitForElementToAppear(buttonLogOut);
        return buttonLogOut.isDisplayed();
    }

    public void logOut() {
        waitForElementToAppear(buttonLogOut);
        buttonLogOut.click();
    }

}
