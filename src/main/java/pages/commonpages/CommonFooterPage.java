package pages.commonpages;

import com.github.javafaker.Faker;
import helper.others.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.corepage.CorePage;

import java.util.Locale;

public class CommonFooterPage extends CorePage {

    public CommonFooterPage(WebDriver driver) {
        super(driver);
    }

    @FindBy (css = "input[name='email']")
    private WebElement inputFieldSubscribe;

    @FindBy (css = "input[name='submitNewsletter']")
    private WebElement inputButtonSubscribe;

    @FindBy (css = "p.alert.alert-danger")
    private WebElement alertInvalidEmailForSubscribe;

    @FindBy (css = "p.alert.alert-success")
    private WebElement alertSuccessfulSubscribe;


    /**
     * Handle empty email address case, and valid email address case.
     * @param withAddress if true correct email address is passed / if false empty string is passed
     * @return current site instance
     */
    public CommonFooterPage signForSubscriptionWithEmail(boolean withAddress) {
        String email = "";

        if (withAddress) {
            Faker faker = new Faker(new Locale("pl-PL"));
            email = faker.internet().safeEmailAddress();
        }
        createCustomerWithEmail(email);
        return this;
    }

    /**
     * Handle case where email address is already present in DB.
     * @param email email
     * @return current site instance
     */
    public CommonFooterPage createCustomerWithEmail(String email) {
        inputFieldSubscribe.clear();
        inputFieldSubscribe.sendKeys(email);
        inputButtonSubscribe.click();
        return this;
    }

    public boolean isAlertInvalidEmailForSubscribeVisibleAndCorrect() {
        String alertText = getAlertText(alertInvalidEmailForSubscribe);

        return alertInvalidEmailForSubscribe.isDisplayed() &&
                alertText.equals("Nieprawidłowy adres e-mail");
    }

    public boolean isAlertSuccessfulSubscribeVisibleAndCorrect(){
        String alertText = getAlertText(alertSuccessfulSubscribe);

        return alertSuccessfulSubscribe.isDisplayed() &&
                alertText.equals("Zarejestrowano do subskrypcji");
    }

    public boolean isAlertAlreadySubscribedVisibleAndCorrect() {
        String alertText = getAlertText(alertInvalidEmailForSubscribe);

        return alertInvalidEmailForSubscribe.isDisplayed() &&
                alertText.equals("Ten email już jest w bazie");
    }

    public String getAlertText(WebElement alert) {
        Waiter.waitForElementToAppear(alert);
        return alert.getText();
    }
}