package pages.loginpage;

import object.pojo.Customer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.corepage.CorePage;
import pages.createaccountpage.CreateAccountPage;

import static helper.others.Waiter.waitForElementToAppear;

public class LogInPage extends CorePage {

    public LogInPage(WebDriver driver) {
        super(driver);
    }

    @FindBy (css = "div.no-account a")
    private WebElement anchorCreateNewAccount;

    @FindBy (css = ".form-control[name='email']")
    private WebElement inputEmail;

    @FindBy (css = "input[name='password']")
    private WebElement inputPassword;

    @FindBy (css = "#submit-login")
    private WebElement buttonLogIn;

    @FindBy (css = ".alert.alert-danger")
    private WebElement alertInvalidCredentials;


    public CreateAccountPage goToCreateAccountPage() {
        anchorCreateNewAccount.click();
        return new CreateAccountPage(getDriver());
    }

    public LogInPage fillFormWithEmailAndPassword(Customer customer) {
        fillFormWithEmailAndPassword(
                customer.getEmail(),
                customer.getPassword()
        );
        return this;
    }

    public LogInPage fillFormWithEmailAndPassword(String email, String password) {
        waitForElementToAppear(inputEmail);
        waitForElementToAppear(inputPassword);
        inputEmail.sendKeys(email);
        inputPassword.sendKeys(password);
        return this;
    }

    public void submitLogIn() {
        buttonLogIn.click();
    }

    public boolean isLogAlertInvalidCredentialsVisibleAndCorrect() {
        waitForElementToAppear(alertInvalidCredentials);
        return alertInvalidCredentials.isDisplayed() &&
                alertInvalidCredentials.getText().equals("Błąd uwierzytelniania.");
    }



}
