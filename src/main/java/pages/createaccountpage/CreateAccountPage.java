package pages.createaccountpage;

import helper.others.Waiter;
import object.pojo.Customer;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.corepage.CorePage;

import java.util.List;

import static helper.others.Waiter.*;

public class CreateAccountPage extends CorePage {

    public CreateAccountPage(WebDriver driver) {
        super(driver);
    }

    @FindBy (css = "input[name='firstname']")
    private  WebElement inputFirstname;

    @FindBy (css = "input[name='lastname']")
    private  WebElement inputLastname;

    @FindBy (css = "div.col-md-6 > input[name='email']")
    private  WebElement inputEmail;

    @FindBy (css = "input[name='password']")
    private  WebElement inputPassword;

    @FindBy (css = "footer > button")
    private  WebElement buttonSave;

    @FindBy (css = "li.alert.alert-danger")
    private List<WebElement> firstnameLastnameValidationMessages;

    @FindBy (css = "input[name='psgdpr']")
    private WebElement inputAcceptPolicies;

    public List<WebElement> getFirstnameLastnameValidationMessages() {
        return firstnameLastnameValidationMessages;
    }

    /**
     * Method to crete fill register form with minimal data neccessary (firstname, lastname, email,password).
     * Passing a NewCustomer object. It is used for positive registration case, pretty much only.
     * @param customer object
     * @return this, cause its the same page
     */
    public CreateAccountPage fillFormWithMinimalData(Customer customer) {
        fillFormWithMinimalData(
                customer.getFirstname(),
                customer.getLastname(),
                customer.getEmail(),
                customer.getPassword()
        );
        return this;
    }

    /**
     * Method to crete fill register form with minimal data neccessary (firstname, lastname, email,password).
     * Passing a String values from dataProvider. Used mainly for Data Driven Tests. Many combinations can be achieved.
     * @param firstname -
     * @param lastname -
     * @param email -
     * @param password -
     * @return this, cause its the same page :)
     */
    public CreateAccountPage fillFormWithMinimalData(String firstname, String lastname, String email, String password) {
        waitForElementToAppear(inputFirstname);
        inputFirstname.sendKeys(firstname);
        inputLastname.sendKeys(lastname);
        inputEmail.sendKeys(email);
        inputPassword.sendKeys(password);
        inputAcceptPolicies.click();
        return this;
    }

    /**
     * Normally there are 2 same validation messages. Method is ready if there will be more same messages in future.
     * In first for loop it checks if all list elements are visible.
     * In second for loop it checks if any on list elements is not displayed. If Any of those is not displayed returns false.
     * !!!!IMPORTANT!!!! I consider cases where firstname and lastname fields are not valid. I do not consider only one
     * of those to be invalid.
     * @return true if all list elements are displayed. false if any of list elements is not displayed.
     */
    public boolean isFirstnameLastnameValidationMessagesVisibleAndCorrect() {
        waitForMessagesToBeVisible();
        return isValidationMessagesVisible() && isValidationMessagesTextCorrect();
    }

    private boolean isValidationMessagesTextCorrect() {
        final String EXPECTED_VALIDATION_MESSAGE_TEXT = "Nieprawidłowa nazwa\n" +
                "Niepoprawne znaki: 0-9!<>,;?=+()@#\"°{}_$%/\\^*`\n" +
                "Po tym wymagane jest miejsce \".\" i \"。\"";

        return firstnameLastnameValidationMessages.stream()
                .map(WebElement::getText)
                .allMatch(x -> x.equals(EXPECTED_VALIDATION_MESSAGE_TEXT));
    }

    private boolean isValidationMessagesVisible() {
        return firstnameLastnameValidationMessages.stream()
                .allMatch(WebElement::isDisplayed);
    }

    private void waitForMessagesToBeVisible() {
        firstnameLastnameValidationMessages.forEach(Waiter::waitForElementToAppear);
    }

    public void submitRegistration(){
        buttonSave.click();
    }


}
