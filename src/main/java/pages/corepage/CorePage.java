package pages.corepage;

import helper.others.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CorePage {

    private WebDriver driver;
    private Waiter waiter;

    public CorePage(WebDriver driver) {
        this.driver = driver;
        this.waiter = new Waiter(driver);
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public Waiter getWaiter() {
        return waiter;
    }
}
