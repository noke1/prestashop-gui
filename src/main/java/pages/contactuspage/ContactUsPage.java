package pages.contactuspage;

import helper.others.Waiter;
import object.pojo.ContactUsMessage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pages.corepage.CorePage;

import static helper.others.Waiter.*;
import static pages.contactuspage.ContactUsPage.ContactUsTopic.BOK;
import static pages.contactuspage.ContactUsPage.ContactUsTopic.WEBMASTER;

public class ContactUsPage extends CorePage {

    public ContactUsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy (css = "select[name='id_contact']")
    private WebElement Select;

    @FindBy (css = "input.form-control[type='email']")
    private WebElement inputEmail;

    @FindBy(css = "textarea")
    private WebElement textAreaMessage;

    @FindBy (css = "input[name='submitMessage']")
    private WebElement inputSubmitMessage;

    @FindBy (css = "span.buttonText")
    private WebElement spanAddAttachment;

    @FindBy (css = ".contact-form ul > li")
    private WebElement alertSuccessfulMessageSend;

    @FindBy (css = ".bootstrap-filestyle.input-group > input")
    private WebElement inputAddAttachment;

    @FindBy (css = ".contact-form ul > li")
    private WebElement alertInvalidAttachmentFormat;

    public enum ContactUsTopic {
        BOK,
        WEBMASTER
    }


    public ContactUsPage fillFormWithMinimalData(ContactUsMessage message){
        waitForElementToAppear(inputEmail);
        inputEmail.sendKeys(message.getEmail());
        textAreaMessage.sendKeys(message.getMessageText());
        return this;
    }

    public ContactUsPage selectMessageTopic(ContactUsTopic contactUsTopic) {
        waitForElementToAppear(Select);
        Select selectSubjects = new Select(Select);

        if (contactUsTopic.equals(BOK)) {
            selectSubjects.selectByVisibleText("Biuro Obsługi Klienta");
        } else if (contactUsTopic.equals(WEBMASTER)) {
            selectSubjects.selectByVisibleText("Webmaster");
        }
        return this;
    }

    public ContactUsPage sendMessage() {
        inputSubmitMessage.click();
        return this;
    }

    public boolean isAlertSuccessfulMessageSendVisibleAndCorrect() {
        String alertText = getAlertText(alertSuccessfulMessageSend);

        return alertSuccessfulMessageSend.isDisplayed() &&
                alertText.equals("Twoja wiadomość została pomyślnie wysłana do obsługi.");
    }

    public boolean isAlertInvalidAttachmentFormatVisibleAndCorrect() {
        String alertText = getAlertText(alertInvalidAttachmentFormat);

        return alertInvalidAttachmentFormat.isDisplayed() &&
                alertText.equals("Nieprawidłowe rozszerzenie pliku");
    }

    public String getAlertText(WebElement alert) {
        waitForElementToAppear(alert);
        return alert.getText();
    }

    public ContactUsPage addAttachment(String pathToAttachment) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("document.querySelector('.bootstrap-filestyle.input-group > input').removeAttribute('disabled');");

        inputAddAttachment.sendKeys(pathToAttachment);
        return this;
    }
}
