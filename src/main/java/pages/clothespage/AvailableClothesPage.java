package pages.clothespage;

import helper.others.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.corepage.CorePage;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static helper.others.Waiter.*;

public class AvailableClothesPage extends CorePage {

    @FindBy (css = ".product-description")
    private List<WebElement> listAvailableClothes;

    @FindBy (css = ".h3.product-title >  a")
    private List<WebElement> listAvailableClothesNames;

    @FindBy (css = "span.price")
    private List<WebElement> listAvailableClothesPrices;

    @FindBy (css = ".dropdown-menu > a")
    private List<WebElement> listSortingTypes;

    @FindBy (css = "button.btn-unstyle.select-title")
    private  WebElement buttonSorting;


    public AvailableClothesPage(WebDriver driver) {
        super(driver);
    }

    public enum SortType {
        DOSTEPNE,
        NAME_ASC,
        NAME_DESC,
        PRICE_ASC,
        PRICE_DESC
    }


    public int countClothesFound() {
        listAvailableClothes.forEach(Waiter::waitForElementToAppear);
        return listAvailableClothes.size();
    }

    public List<String> getClothesNames() {
        return listAvailableClothesNames.stream()
                .map(cloth -> cloth.getText().toLowerCase())
                .collect(Collectors.toList());
    }

    public List<Double> getClothesPrices() {
        return listAvailableClothesPrices.stream()
                .map(cloth -> getFormattedPrice(cloth.getText()))
                .collect(Collectors.toList());
    }

    public static <T> List<T> reverseList(List<T> toReverse) {
        Collections.reverse(toReverse);
        return toReverse;
    }

    public double getFormattedPrice(String text) {
        return Double.parseDouble(text.
                replace("zł","").
                replace(",",".").
                replace(" ","")
        );
    }

    public AvailableClothesPage sortClothesBy(SortType type){
        switch (type) {
            case DOSTEPNE:
                chooseSortOption(0, listSortingTypes);
                waitForClothes();
                break;
            case NAME_ASC:
                chooseSortOption(1, listSortingTypes);
                waitForClothes();
                break;
            case NAME_DESC:
                chooseSortOption(2, listSortingTypes);
                waitForClothes();
                break;
            case PRICE_ASC:
                chooseSortOption(3, listSortingTypes);
                waitForClothes();
                break;
            case PRICE_DESC:
                chooseSortOption(4, listSortingTypes);
                waitForClothes();
                break;
        }
        return this;
    }

    /**
     * Needs to click sorting button first. ONLY After clicking this button, sorting types are displayed.
     * @param sortOption - sorting type position on the list.
     * @param list - dropdown list which show after clicking button
     * @return
     */
    public void chooseSortOption(int sortOption, List<WebElement> list) {
        waitForElementToAppear(buttonSorting);
        buttonSorting.click();

        WebElement position = list.get(sortOption);
        waitForElementToAppear(position);
        position.click();
    }

    public void waitForClothes() {
        listAvailableClothes.forEach(Waiter::waitForStaleElementToAppear);
    }

}