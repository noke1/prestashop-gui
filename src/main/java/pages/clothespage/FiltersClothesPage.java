package pages.clothespage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.corepage.CorePage;

import java.util.List;

import static helper.others.Waiter.waitForElementToAppear;

public class FiltersClothesPage extends CorePage {

    @FindBy (css = ".category-sub-menu li a")
    private List<WebElement> listSubcategories;

    public enum SubCategory {
        MEN,
        WOMEN
    }

    public FiltersClothesPage(WebDriver driver) {
        super(driver);
    }

    public FiltersClothesPage goToSubcategory(SubCategory subCategory) {
        switch (subCategory) {
            case MEN:
                chooseCategoryOption(0, listSubcategories);
                break;
            case WOMEN:
                chooseCategoryOption(1, listSubcategories);
                break;
        }
        return this;
    }

    public void chooseCategoryOption(int categoryOption, List<WebElement> list) {
        waitForElementToAppear(list.get(categoryOption));
        list.get(categoryOption).click();
    }

}
