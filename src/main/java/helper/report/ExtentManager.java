package helper.report;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import java.io.FileNotFoundException;

public class ExtentManager {

    private ExtentReports extentReports;
    private ExtentReportPathProvider extentReportPathProvider = new ExtentReportPathProvider();

    public ExtentReports getInstance() throws FileNotFoundException {
        if (extentReports == null) createInstance();
        return extentReports;
    }

    /**
     * Create instance for extentReporter.
     * @return
     */
    private void createInstance() throws FileNotFoundException {
        final String REPORT_FULL_PATH = extentReportPathProvider.getREPORT_FULL_PATH();
        ExtentHtmlReporter extentHtmlReporter = new ExtentHtmlReporter(REPORT_FULL_PATH);

        setExtentHtmlReporterSettings(extentHtmlReporter);
        setExtentReportsSettings(extentHtmlReporter);
    }

    private void setExtentReportsSettings(ExtentHtmlReporter reporterName) {
        extentReports = new ExtentReports();
        extentReports.attachReporter(reporterName);

        extentReports.setSystemInfo("OS", "Windows");
        extentReports.setSystemInfo("AUT", "QA");
    }

    private void setExtentHtmlReporterSettings(ExtentHtmlReporter reporterName) {
        final String REPORT_FILE_NAME = extentReportPathProvider.getREPORT_FILE_NAME();

        reporterName.config().setEncoding("utf-8");
        reporterName.config().setChartVisibilityOnOpen(true);
        reporterName.config().setTheme(Theme.DARK);
        reporterName.config().setTestViewChartLocation(ChartLocation.BOTTOM);
        reporterName.config().setDocumentTitle(REPORT_FILE_NAME);
        reporterName.config().setReportName(REPORT_FILE_NAME);
        reporterName.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");
    }



}
