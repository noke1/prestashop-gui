package helper.report;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class ExtentTestManager {

    private Map<Integer, ExtentTest> extentTestMap = new HashMap<>();
    private ExtentManager extentManager = new ExtentManager();
    private ExtentReports extentReports;

    public synchronized ExtentTest getTest() {
        return extentTestMap.get((int) Thread.currentThread().getId());
    }

    public synchronized void endTest() throws FileNotFoundException {
        extentReports = extentManager.getInstance();
        extentReports.flush();
    }

    public synchronized void startTest(String testName) throws FileNotFoundException {
        extentReports = extentManager.getInstance();
        ExtentTest test = extentReports.createTest(testName);
        extentTestMap.put((int) Thread.currentThread().getId(), test);
    }
}
