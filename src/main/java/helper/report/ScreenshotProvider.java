package helper.report;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class ScreenshotProvider {

    private ExtentReportPathProvider extentReportPathProvider = new ExtentReportPathProvider();
    private final String SCREENSHOT_FOLDER = extentReportPathProvider.getEACH_REPORT_FOLDER();
    private static final Logger LOGGER = LogManager.getLogger(TestListener.class.getName());

    public String getScreenShotPath(WebDriver driver, String screenshotName)  {
        TakesScreenshot ts = (TakesScreenshot)driver;
        //create File object variable which holds the screen shot reference
        File source = ts.getScreenshotAs(OutputType.FILE);
        //store the screen shot path in path variable. Here we are storing the screenshots under screenshots folder
        String path = String.format("%s%s.png", SCREENSHOT_FOLDER, screenshotName);
        //create another File object variable which points(refer) to the above stored path variable
        File destination = new File(path);
        //use FileUtils class method to save the screen shot at desired path
        try {
            FileUtils.copyFile(source, destination);
        } catch (IOException e) {
            String stacktrace = ExceptionUtils.getStackTrace(e);
            LOGGER.error(String.format("Something went wrong and screenshot was not saved in destination: %s", destination));
            LOGGER.error("Stacktrace: " + stacktrace);
        }

        return path;
    }

    public void addScreenShotToMethod(ExtentTestManager extentTestManager, String savePath) {
        try {
            extentTestManager.getTest().addScreenCaptureFromPath(savePath);
        } catch (IOException e) {
            String stacktrace = ExceptionUtils.getStackTrace(e);
            LOGGER.error(String.format("Something went wrong and screenshot was not attached to report. " +
                    "Was searching for screenshot inside directory: %s", savePath));
            LOGGER.error("Stacktrace: " + stacktrace);
        }
    }
}
