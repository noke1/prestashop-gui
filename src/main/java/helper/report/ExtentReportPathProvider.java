package helper.report;

import helper.DateProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;

public class ExtentReportPathProvider {

    //Messy, messy, messy... :)
    private final String CURRENT_DATE = new DateProvider().getCurrentDate();
    private final String CURRENT_DATE_AND_TIME = new DateProvider().getCurrentDateAndTime();
    private final String FILE_SEPARATOR = System.getProperty("file.separator");
    private final String USER_DIRECTORY = System.getProperty("user.dir") + FILE_SEPARATOR;
    private final String MAIN_REPORT_FOLDER = USER_DIRECTORY + "reports_folder"+ FILE_SEPARATOR;
    private final String DAILY_REPORT_FOLDER = MAIN_REPORT_FOLDER + CURRENT_DATE + FILE_SEPARATOR;
    private final String EACH_REPORT_FOLDER = DAILY_REPORT_FOLDER + "Report-" + CURRENT_DATE_AND_TIME + FILE_SEPARATOR;
    private final String REPORT_FILE_NAME = "Test-Automaton-Report-"+ CURRENT_DATE_AND_TIME + ".html";
    private final String REPORT_FULL_PATH =  EACH_REPORT_FOLDER + REPORT_FILE_NAME;
    private static final Logger LOGGER = LogManager.getLogger(ExtentReportPathProvider.class.getName());

    /**
     * Get report creation path.
     * @return
     */
    public String getREPORT_FULL_PATH() throws FileNotFoundException {
        createFolder(MAIN_REPORT_FOLDER);
        createFolder(DAILY_REPORT_FOLDER);
        createFolder(EACH_REPORT_FOLDER);

        return REPORT_FULL_PATH;
    }

    private void createFolder(String folderPath) throws FileNotFoundException {
        File testDirectory = new File(folderPath);

        if (testDirectory.exists()) {
            LOGGER.warn(String.format("Folder %s already exists!", folderPath));
            return;
        }

        if (testDirectory.mkdir()) {
            LOGGER.info(String.format("Folder: %s is created!", folderPath));
        } else {
            LOGGER.fatal(String.format("Creation of folder: %s failed!", folderPath));
            throw new FileNotFoundException();
        }
    }

    public String getREPORT_FILE_NAME() {
        return REPORT_FILE_NAME;
    }

    public String getEACH_REPORT_FOLDER() {
        return EACH_REPORT_FOLDER;
    }
}
