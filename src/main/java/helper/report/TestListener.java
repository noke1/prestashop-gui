package helper.report;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.FileNotFoundException;

public class TestListener implements ITestListener {

    private ExtentTestManager extentTestManager = new ExtentTestManager();
    private ExtentManager extentManager = new ExtentManager();
    private ScreenshotProvider screenshotProvider = new ScreenshotProvider();
    private static final Logger LOGGER = LogManager.getLogger(TestListener.class.getName());

    public void onStart(ITestContext context) {
        String suiteName = context.getName();

        LOGGER.trace(String.format("***Test Suite %s started***", suiteName));
    }

    public void onFinish(ITestContext context) {
        String suiteName = context.getName();

        LOGGER.trace(String.format("***Test Suite %s finish***", suiteName));

        try {
            extentTestManager.endTest();
            //when below line was active framework tried to create reports folder on test start and on test finish.
            //we don't want to create reports folder on test finish.
            //extentManager.getInstance().flush();
        } catch (FileNotFoundException e) {
            String stacktrace = ExceptionUtils.getStackTrace(e);
            LOGGER.error(String.format("Test suite: %s finish failed.***", suiteName));
            LOGGER.error("Stacktrace: " + stacktrace);
        }
    }

    public void onTestStart(ITestResult result) {
        String methodName = result.getMethod().getMethodName();

        LOGGER.trace(String.format("Running test method: %s.", methodName));

        try {
            extentTestManager.startTest(methodName);
        } catch (FileNotFoundException e) {
            String stacktrace = ExceptionUtils.getStackTrace(e);
            LOGGER.error(String.format("Test execution of method: %s failed.", methodName));
            LOGGER.error("Stacktrace: " + stacktrace);
        }
    }

    public void onTestSuccess(ITestResult result) {
        String methodName = result.getMethod().getMethodName();

        LOGGER.info(String.format("Executed method: %s successfully.", methodName));
        extentTestManager.getTest().log(
                Status.PASS,
                MarkupHelper.createLabel("SUCCESS", ExtentColor.GREEN)
        );
    }

    public void onTestFailure(ITestResult result) {
        //imported form CoreTest class so i can make a screenshot and attach it to report.
        WebDriver driver = (WebDriver) result.getTestContext().getAttribute("webDriver");
        String methodName = result.getMethod().getMethodName();

        LOGGER.error(String.format("Test execution %s failed.", methodName));
        extentTestManager.getTest().log(
                Status.FAIL,
                MarkupHelper.createLabel("FAILED", ExtentColor.RED)
        );
        //Add assertions results to report
        extentTestManager.getTest().fail(result.getThrowable());

        String screenShotPath = screenshotProvider.getScreenShotPath(driver, methodName);
        screenshotProvider.addScreenShotToMethod(extentTestManager,screenShotPath);
    }

    public void onTestSkipped(ITestResult result) {
        String methodName = result.getMethod().getMethodName();

        LOGGER.warn(String.format("Test %s skipped...", methodName));
        extentTestManager.getTest().log(
                Status.SKIP,
                MarkupHelper.createLabel("SKIPPED", ExtentColor.YELLOW)
        );
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        String methodName = result.getMethod().getMethodName();

        LOGGER.info(String.format("Test failed but within percentage %s ", methodName));
    }

}
