package helper.excel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;

public class ExcelReader {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private XSSFCell cell;
    private XSSFRow row;
    private static final Logger LOGGER = LogManager.getLogger(ExcelReader.class.getName());

    public Object[][] getTestDataFromExcel(final String PATH_TO_EXCEL_FILE, final String SHEET_NAME) throws IOException {
        FileInputStream excelFile = new FileInputStream(PATH_TO_EXCEL_FILE);
        LOGGER.info(String.format("File %s was found in directory.", PATH_TO_EXCEL_FILE));

        workbook = new XSSFWorkbook(excelFile);
        LOGGER.info(String.format("Workbook with number of sheets %d was correctly initialized.", workbook.getNumberOfSheets()));

        sheet = workbook.getSheet(SHEET_NAME);
        LOGGER.info(String.format("Using sheet named: %s", sheet.getSheetName()));

        row = sheet.getRow(0);
        LOGGER.info(String.format("Initialized row at number: %d", row.getRowNum()));

        int totalNumberOfRows = sheet.getPhysicalNumberOfRows();
        LOGGER.info(String.format("Found %d rows in current sheet %s. One of them is Header row!!!", totalNumberOfRows, sheet.getSheetName()));
        int totalNumberOfColumns = row.getLastCellNum();
        LOGGER.info(String.format("Found %d columns in current sheet %s", totalNumberOfColumns, sheet.getSheetName()));

        return transferDataFromExcelToArray(totalNumberOfRows, totalNumberOfColumns);
    }

    private String[][] transferDataFromExcelToArray(int totalNumberOfRows, int totalNumberOfColumns) {
        String[][] data = new String[totalNumberOfRows -1][totalNumberOfColumns];

        for (int i = 1; i < totalNumberOfRows; i++) {
            for (int j = 0; j < totalNumberOfColumns; j++) {
                row = sheet.getRow(i);
                cell = row.getCell(j);
                data[i-1][j] = cell.getStringCellValue();
                LOGGER.info(String.format("Reading data from excel at row [%d], column [%d] - value: %s", i+1, j+1, cell.getStringCellValue()));
            }
        }
        return data;
    }
}
