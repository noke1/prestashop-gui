package helper.others;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {

    private static WebDriverWait wait;
    private static WebDriver driver;
    private static final int TIMEOUT = 5;
    private static final int POLLING = 100;

    public Waiter(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, TIMEOUT, POLLING);
    }

    public static void waitForElementToAppear(WebElement locator) {
        wait.ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.refreshed((ExpectedConditions.visibilityOf(locator))));
    }

    /**
     * 1. Enter to a page - web elements are initialized, 2. click button located by @findBy, 3. refresh page,
     * 4. Click the same button again, 5. StaleElementReference Exception!!! This is why this method was created.
     * @param locator
     */
    public static void waitForStaleElementToAppear(WebElement locator) {
        wait.ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.refreshed((ExpectedConditions.stalenessOf(locator))));
    }

    public void waitForElementToDisappear(WebElement locator) {
        wait.until(ExpectedConditions.invisibilityOf(locator));
    }
}
