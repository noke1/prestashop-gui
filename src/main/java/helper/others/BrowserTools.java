package helper.others;

import org.openqa.selenium.WebDriver;

public class BrowserTools {

    private WebDriver driver;

    public BrowserTools(WebDriver driver) {
        this.driver = driver;
    }

    public void goTo(String url){
        driver.get(url);
    }
}
