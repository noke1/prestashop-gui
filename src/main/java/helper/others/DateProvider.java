package helper;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateProvider {

    private Date now = new Date();

    public String getCurrentDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(now);
    }

    public String getCurrentDateAndTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
        return simpleDateFormat.format(now);
    }

}
