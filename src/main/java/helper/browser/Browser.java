package helper.browser;

import org.openqa.selenium.WebDriver;


public class Browser {

    public Initialize initialize;
    public Terminate terminate;
    public Adjust adjust;
    public String name;

    public Browser(String name, Initialize initialize, Terminate terminate, Adjust adjust) {
        this.name = name;
        this.initialize = initialize;
        this.terminate = terminate;
        this.adjust = adjust;
    }

    public WebDriver initialize() {
        return this.initialize.initialize();
    }

    public void terminate(WebDriver driver) {
        this.terminate.terminate(driver);
    }

    public void adjust(WebDriver driver) {
        this.adjust.adjust(driver);
    }
}
