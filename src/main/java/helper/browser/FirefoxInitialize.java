package helper.browser;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxInitialize implements Initialize {

    @Override
    public WebDriver initialize() {
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver();
    }
}
