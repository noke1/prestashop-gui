package helper.browser;

import org.openqa.selenium.WebDriver;

public class BrowserAdjust implements Adjust {


    @Override
    public void adjust(WebDriver driver) {
        prepareBrowser(driver);
        goToApplicationUrl(driver);
    }

    private void prepareBrowser(WebDriver driver) {
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
    }

    private void goToApplicationUrl(WebDriver driver) {
        driver.get(APPLICATION_URL);
    }


}
