package helper.browser;

import org.openqa.selenium.WebDriver;

public interface Terminate {

    public void terminate(WebDriver driver);
}
