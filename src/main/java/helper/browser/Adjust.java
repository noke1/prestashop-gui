package helper.browser;

import org.openqa.selenium.WebDriver;

public interface Adjust {

    String APPLICATION_URL = "http://localhost:8080";

    void adjust(WebDriver driver);
}
