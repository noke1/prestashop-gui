package helper.browser;

import org.openqa.selenium.WebDriver;

public interface Initialize {
    public WebDriver initialize();
}
