package helper.browser;

import org.openqa.selenium.WebDriver;

/**
 * Browser terminate is same for each helpers.browser. It doesnt mind if its Chrome or Firefox. Every future driver
 * will call this termination class.
 */

public class BrowserTerminate implements Terminate {

    @Override
    public void terminate(WebDriver driver) {
        driver.close();
        driver.quit();
    }
}
