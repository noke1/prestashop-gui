package helper.browser;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeInitialize implements Initialize {

    @Override
    public WebDriver initialize() {
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    }

}
